(function($) {
    "use strict"; // Start of use strict


   
    

    $('.slider').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true
    });
    $('.slick-vertical').slick({
        infinite: true,
        autoplay: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        vertical: true
        
    });


    if ($(".btn-top").length > 0) {
        $(window).scroll(function() {
            var e = $(window).scrollTop();
            if (e > 100) {
                $(".btn-top").show()
            } else {
                $(".btn-top").hide()
            }
        });
        $(".btn-top").click(function() {
            $('body,html').animate({
                scrollTop: 0
            })
        });
    }

})(jQuery); // End of use strict







